<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="copyright" content="Gerard Hickey, WT0F">
        <title>MeshPhone White Pages</title>
        <link rel="stylesheet" href="whitepages.css">
    </head>
    <body>
        <h2>MeshPhone WhitePages</h2>
        <table id="info">
            <tr><td>
                <div id="search_box">
                    <form method="post" action="">
                        <input type="text" id="search" name="search"
                               placeholder="Search call sign, name, phone number, PBX or notes">
                        <input type="submit" value="Search">
                        <input type="submit" value="Show all listings">
                    </form>
                </div>
            </td><td rowspan="2">
                <div class="project_info">
            Project repository: <a href="https://gitlab.com/aredn-apps/whitepages/"
                                   target="new">https://gitlab.com/aredn-apps/whitepages/</a>
                </div>
                <br/>
                <div class="project_info">
                    Written by Gerard Hickey, WT0F
                </div>
                <br/>
                <div class="project_info">
                    Original concept by Mark Herson, N2MH
                </div>
            </td></tr>
            <tr><td>
                <div>
                    WhitePages directory listings can be updated using
                    N2MH's self service portal. Once a directory listing
                    is updated or created it can take some time for the
                    entry to show up here.
                </div>
                <br/>
                <apan id="self_service_portal">
                    <a href="http://n2mh-meshphone2/meshutil/wpsearch.php" target="new">
                            WhitePages Self Service</a>
                </span>
            </td></tr>
        </table>

<?php
// Load XML file
$xml = simplexml_load_file('whitepages.xml');

// Check if the XML file is loaded successfully
if ($xml) {
    // Filter data based on search term
    $searchTerm = isset($_POST['search']) ? trim($_POST['search']) : '';

    // Create a table
    create_table_headers();

    // Loop through each <listing> element in the XML data
    foreach ($xml->listing as $listing) {
        // Check if the search term matches any column value
        if (empty($searchTerm) ||
        stripos($listing->callsign, $searchTerm) !== false ||
            stripos($listing->extension, $searchTerm) !== false ||
            stripos($listing->opname, $searchTerm) !== false ||
            stripos($listing->pbx, $searchTerm) !== false ||
            stripos($listing->meshphone, $searchTerm) !== false ||
            stripos(urldecode($listing->notes), $searchTerm) !== false ||
            stripos($listing->meshmail, $searchTerm) !== false
        ) {
            // Display matching row
            render_row($listing);
        }
    }
    end_table();
} else {
    // Display an error if the XML file cannot be loaded
    echo '<p>Error loading XML file</p>';
}

function create_table_headers() {
    $html = <<<END_OF_HTML
    <table id="directory_listings">
    <tr>
        <th>Call Sign</th>
        <th>Operator's Name</th>
        <th>Local Extension</th>
        <th>Local PBX</th>
        <th>MeshPhone Number (78+ this number)</th>
        <th>Local time number (78+ this number)</th>
        <th>DMR ID (70+ this number) (Future)</th>
        <th>MeshMail email address</th>
        <th>Notes</th>
    </tr>
    END_OF_HTML;
    echo $html;
}

function render_row($data) {
    $html = '<tr class="listing">';
    $html .= '<td>' . $data->callsign . '</td>';
    $html .= '<td>' . $data->opname . '</td>';
    $html .= '<td>' . $data->extension . '</td>';
    $html .= '<td>' . $data->pbx . '</td>';
    $html .= '<td>' . $data->meshphone . '</td>';
    $html .= '<td>' . $data->meshtime . '</td>';
    $html .= '<td>' . $data->dmr_id . '</td>';
    $html .= '<td>' . $data->meshmail . '</td>';
    $html .= '<td>' . urldecode($data->notes) . '</td>';
    $html .= '</tr>';
    echo $html;
}

function end_table() {
    echo "</table>";
}
?>
        <footer id="copyright" property="dc:rights">
            &copy; Copyright
            <span property="dc:dateCopyrighted"><?php echo date("Y"); ?></span>,
            <span property="dc:publisher">Gerard Hickey, WT0F</span>
        </footer>
    </body>
</html>
