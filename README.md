MeshChat White Pages
====================

This is a simple single page application to provide access to the
MeshPhone White Pages directory. It does not implement all the functionality
of N2MH's White Pages--just the viewing and searching of the directory data.
To add new entries or update existing entries, it is required that one use
the self-service link on the N2MH White Pages site.

Installation
------------

There are two files that need to be put in the htdocs root directory
(typically `/var/www/html`) to display the data and create a cron job
to periodically retrieve the XML data that contains all the White Pages
data.

It is generally suggested that the files be installed on a FreePBX
installation as this distribution already has a PHP enabled web server.
There is nothing preventing the files from being installed in another
environment, but it is left to the administrator to determine the location
of the htdocs root directory.

1. Copy the `whitepages.php` and `whitepages.css` files to the htdocs
   root directory
      * [whitepages.css](https://gitlab.com/aredn-apps/whitepages/-/raw/main/whitepages.css?ref_type=heads)
      * [whitepages.php](https://gitlab.com/aredn-apps/whitepages/-/raw/main/whitepages.php?ref_type=heads)
2. Create the following cron job by adding the following lines to
   `/etc/cron.hourly/whitepages`:

   ```
   #!/bin/sh

   cd /var/www/html
   curl --fail --remote-time -sLo data.xml http://n2mh-meshphone2.local.mesh/meshphone/wpxml.xml && mv data.xml whitepages.xml
   ```
3. Ensure that `/etc/cron.hourly/whitepages` is executable: `chmod 755
   /etc/cron.hourly/whitepages`